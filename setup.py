import cx_Freeze
import sys
from PIL import ImageTk, Image
import os

base = None

if sys.platform == 'win32':
    base = "Win32GUI"

executables = [cx_Freeze.Executable("main.py", base=base, icon="images/favicon.ico")]

cx_Freeze.setup(
    name = "SimCalc",
    options = {"build_exe": {"packages":["tkinter","PIL"], "include_files":["images/config.png", "images/favicon.ico"]}},
    version = "0.01",
    description = "SimCalc - By Dani",
    executables = executables
    )
