# All needed liveries import
from tkinter import *
from tkinter import ttk, messagebox
from tkinter import messagebox as tkMessageBox
import tkinter.font as tkFont
from tkinter.simpledialog import askinteger
import math
from math import sqrt
import sys

# Mine Liveries
from apps.tod_toc import tod_toc
from apps.a320 import a320
from apps.units_converter import *
from apps.metar_decoder import metar_decoder
# from apps.rich_presence import *

root = Tk()
miframe = Frame(root)
miframe.pack()

# Colors Theme
black_theme = ("#212121")
pink = ("#9f328c")

# Defining fonts
titleStyle = tkFont.Font(family="Lucida Grande", size=35, weight="bold")
subtitleStyle = tkFont.Font(family="Lucida Grande", size=18, weight="bold")
mainButtonStyle = tkFont.Font(family="Lucida Grande", size=14, weight="bold")
darkStyle = tkFont.Font(family="Arial", size=12)

# Window ratio/external aspect
root.title("SimCalc")
root.geometry("820x550")
root.configure(bg=black_theme)
root.resizable(False, False)

'''
Here we try to show the favicon in the window
except: as some SO are not able to show the favicon we dont show it (Linux based sistems, MacOs)
'''

try:
    root.iconbitmap("images/favicon.ico")

except:
    print("Not in windows")

'''This call functions are made for, closing the root window and calling the diferent
windows functions'''

# CALL FUNCTIONS

def a320_call():
    a320()

def unitconverter_call():
    units_converter()

def tod_toc_call():
    tod_toc()

def metar_decoder_call():
    metar_decoder()

# MAIN WINDOW
logotext = PhotoImage(file="images/logotext.png")
intro = Label(root, image=logotext, font=titleStyle, bg=black_theme, fg="white").place(x=270, y=30)

'''Here we are creating all buttons needed for opening the diferent program windows'''

# Buttons
a320Button = Button(root, text="A320 Take Off Speeds Calculator", command=a320_call,
                    bg=pink, fg="white", font=mainButtonStyle).place(x=260, y=200)  # LAUNCHES A320 PAGE
unitsButton = Button(root, text="Units Converter", command=unitconverter_call, bg=pink,
                     fg="white", font=mainButtonStyle).place(x=340, y=270)  # LAUNCHES UNITS CONVERTER PAGE
a320Button = Button(root, text="Top of Descent / Climb Calculator", command=tod_toc_call, bg=pink,
                    fg="white", font=mainButtonStyle).place(x=260, y=350)  # LAUNCHES TOP OF DESCENT AND CLIMB PAGE
metarButton = Button(root, text="Metar Decoder", command=metar_decoder_call, bg=pink,
                     fg="white", font=mainButtonStyle).place(x=345, y=415)  # LAUNCHES METAR DECODER PAGE

# CREDITS

def credits():
    cr = Label(root, text=("DaniGracia, ZeykaFX ©"), font="Arial 12", bg=black_theme, fg="white").place(x=345, y=510)

# RUNING PROGRAM FUNCTIONS
credits()  # CREDITS

# End
root.mainloop()
