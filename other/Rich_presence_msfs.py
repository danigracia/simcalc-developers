from SimConnect import *
from pypresence import Presence
import requests
import datetime

# default modules
import os
import time
import json


def getAirport(lat, lon):  # TODO maybe print the metar or do something with it
    """ 
    this uses the checkwx api to grab the nearest metar and just extract the icao code,
    it should work for most towered airports.
    pre: Lat, Lon are floats or ints
    post: returns the ICAO code of the nearest airport with a metar"""
    url = f'https://api.checkwx.com/metar/lat/{lat}/lon/{lon}'
    hdr = {"X-API-Key": "TOKEN_GOES_HERE"}
    req = requests.get(url, headers=hdr)

    resp = json.loads(req.text)
    decoded_data = json.loads(json.dumps(resp, indent=1))
    station_id = decoded_data['data'][0][:4]
    return station_id


def simconnection():
    """ 
    Checks the connection to flight sim 2020
    pre:/
    post: returns True if flight sim 2020 is running and False if it is not """
    try:
        sm = SimConnect()
        ae = AircraftEvents(sm)
        aq = AircraftRequests(sm, _time=10)
        print('Connected to MSFS')
        return True
    except ConnectionError as e:
        return False


# Discord rpc stuff
client_id = '792374275121217546'
RPC = Presence(client_id, pipe=0)
RPC.connect()
idleTime = datetime.datetime.now().timestamp()  # the  rpc uses the idle time for the rest of the execution somehow
RPC.update(
    large_image="logo_512", large_text='Microsoft Flight Simulator',
    details='Idling', start=idleTime
)

StartTime = datetime.datetime.now().timestamp()  # gets the start time in epoch time, i could also use time.time() to
# get it

if __name__ == '__main__':
    if not simconnection():  # if the sim is not running, start it, here it starts it in "fast lauch mode"
        print('starting msfs')
        EXTERNAL_PROGRAM = r'C:\Windows\System32\cmd.exe /C start ' \
                           r'shell:AppsFolder\Microsoft.FlightSimulator_8wekyb3d8bbwe!App "-FastLaunch" '
        os.system(EXTERNAL_PROGRAM)

    while True:
        try:
            try:
                sm = SimConnect()
                ae = AircraftEvents(sm)
                aq = AircraftRequests(sm, _time=10)
            except ConnectionError as e:
                print(f'MSFS is not detected, "{e}"')
                time.sleep(10)
                continue
            # Grabs alt, lat, lon from SimConnect
            altitude = aq.get("PLANE_ALTITUDE")
            lat = aq.get("PLANE_LATITUDE")
            lon = aq.get("PLANE_LONGITUDE")

            plane = aq.get('TITLE')  # grabs the Plane's name (e.g: Cessna 152 Asobo)
            plane = str(plane)
            plane = plane[2:-1]  # removed the b'...' from the plane's name

            freq = aq.get('COM_ACTIVE_FREQUENCY:1')  # grabs the active COM1 frequency
            on_ground = aq.get('SIM_ON_GROUND')  # 1.0 if the plane is on the ground, 0.0 if its flying

        except NameError as e:  # not sure if this is still usefull...
            print('NameError: ', e)
            time.sleep(10)
            continue

        # ---------------------------------------------------------------------------

        try:
            try:
                city = getAirport(lat, lon)
            except KeyError as e:
                print(f'Lat and lon were equal to NaN somehow: "{e}"')
                continue

        except TypeError as e:  # not sure why i get a type error here
            time.sleep(15)
            continue

        # except IndexError as e: # used with the maps api, if at the coords 0.000, 0.000 means player is in the menu
        # RPC.update(large_image = "logo_512",large_text='Microsoft Flight Simulator', details = 'In the menus',
        # start = StartTime) # update the rich presence to reflect that print('in the menus\n') time.sleep(15) continue

        if city == "DGAA":  # this is the closest airport to the coordinates 0.000,0.000 so it most probably means
            # the player is in the menus
            RPC.update(large_image="logo_512", large_text='Microsoft Flight Simulator', details='In the menus',
                       start=StartTime)  # update the rich presence to reflect that
            print('in the menus\n')
            time.sleep(15)
            continue

        if on_ground == 1.0 and city != "DGAA":  # if on the ground, update rich presence to say in the ground at {city}
            RPC.update(large_image="logo_512", large_text='Microsoft Flight Simulator', details=f'On the ground',
                       state=f'At {city}')
            print(f'on the ground at {city}\n')

        try:
            if on_ground == 0.0:  # if not on the ground then flying in {city} at {altitude} feet
                RPC.update(
                    large_image="logo_512", large_text='Microsoft Flight Simulator',
                    details=f"Flying in {city}", state=f'At around {int(altitude)} feet', start=StartTime
                )
                print(f'flying in {city}, at {int(altitude)} feet\n')

        except TypeError as e:  # comes from int(altitude) i believe
            print(e)
            continue

        time.sleep(15)  # cycle the rich presence

        try:
            RPC.update(
                large_image="logo_512", large_text='Microsoft Flight Simulator',
                details=f"Listening to {str(round(freq, 3))}", state=f'Flying the {str(plane)}', start=StartTime
            )
            print(f'Listening to {round(freq, 3)}, flying the {str(plane)}\n')
        except TypeError as e:
            print(e)
            continue

        time.sleep(15)
