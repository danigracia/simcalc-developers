# All needed liveries import
from tkinter import *
from tkinter import ttk
from tkinter import messagebox as tkMessageBox
import tkinter.font as tkFont
from tkinter.simpledialog import askinteger
import math
from math import sqrt
import os
import sys


def a320():

    # Colors Theme
    black_theme = ("#212121")
    pink = ("#9f328c")

    #Window Configuration
    a320_window = Toplevel() 
    a320_window.title("A320 Take Off Speeds Calculator")
    a320_window.geometry("920x650")
    a320_window.configure(bg=black_theme)
    a320_window.resizable(False, False)
    
    '''
    Here in the try:
    [Try]: to create a iconbit map
    [Except]: Dont create it 

    Why? Because some SO dont support the iconbitmap such as Linux and MacOs
    '''

    try:
        a320_window.iconbitmap("images/favicon.ico")

    except:
            print ("Not in windows")

    def velocity():

        '''
        Function velocity is the one for all window to work. 
        Needs: a320_window to be defined as Tk
        '''


        #TITLE
        label = Label(a320_window, text=("A320 Take Off Speeds Calculator"), font="LucidaGrande 24 bold",bg=black_theme, fg=pink).place(x=200, y=50)

        #All fake labels xd:
        label = Label(a320_window, text="Airport ICAO:",font="Arial 12",bg=black_theme, fg="white").place(x=50, y=150)
        label = Label(a320_window, text="TakeOff Runway (ex. 25L):",font="Arial 12",bg=black_theme, fg="white").place(x=50, y=180)
        label = Label(a320_window, text="Airport Elevation (ft):",font="Arial 12",bg=black_theme, fg="white").place(x=50, y=210)

        label = Label(a320_window, text="Anti Ice:",font="Arial 12",bg=black_theme, fg="white").place(x=50, y=240)
        label = Label(a320_window, text="Runway State:",font="Arial 12",bg=black_theme, fg="white").place(x=50, y=270)

        #All fake entries xd:
        entry = Entry(a320_window, font="Arial 12",bg="white").place(x=250, y=150)
        entry = Entry(a320_window, font="Arial 12",bg="white").place(x=250, y=180)
        entry = Entry(a320_window, font="Arial 12",bg="white").place(x=250, y=210)

        #Fake Combox
        runwaystate=["Dry","Wet"]
        antiices=["Off","Engine", "All"]

        box1 = ttk.Combobox(a320_window, state = 'readonly', values = antiices, width=27)
        box1.set(antiices[0])
        box1.place(x=250, y=240)
        box2 = ttk.Combobox(a320_window, state = 'readonly', values = runwaystate, width=27)
        box2.set(runwaystate[0])
        box2.place(x=250, y=270)

        '''
        This function is used for the a320 take off speeds reference. 
        We need:
        [FlapsList]: Use for show to the user the different options. 
        [FlapsMapping]: Use for associate the options (keys) with the different values of V1, Vr and V3
        def weight_get returns the calculation of the V1, Vr and V3 and is printed in the screen
        '''
        FlapsList = ['1+F', '2', '3']
        FlapsMaping = {'1+F' : 1, '2' : 2, '3' : 3}
        
        weightentry = Entry(a320_window, font="Arial 12",bg="white")
        weightentry.place(x=250, y=330)

        def tow_help():
            label = Label(a320_window, text=("HOW TO OBTAIN TOW"), font="LucidaGrande 14 bold",bg=black_theme, fg="white").place(x=600, y=120)
            fmc = PhotoImage(file="images/fmc_init_small.png")
            label = Label(a320_window, image=fmc)
            label.photo = fmc
            label.place(x=550, y=150)

            def tow_help_destroy():
                label.destroy()
            

        def weight_get():

            '''
            [Flapsx Dictionary]: Use for store the different "Take Off Speeds" Values and the WEIGHT reference. 
            - First I get the selection option of the list and check the input weight
            - Next I check what option is selected and customize the calc for the right dictionary
            (The format of the value of the key weight is a list that the first number is the little and next the more bigger)
            '''

            Flap1 = [{"weight": [72001,100000], "V1": 138, "Vr": 156, "V2": 159 }, {"weight": [67001,72000], "V1": 132, "Vr": 150, "V2": 153 }, 
            {"weight": [63001,67000], "V1": 127, "Vr": 144, "V2": 147 },{"weight": [59001,63000], "V1": 122, "Vr": 139, "V2": 142 },
            {"weight": [56001,59000], "V1": 117, "Vr": 135, "V2": 138 },{"weight": [54001,56000], "V1": 116, "Vr": 133, "V2": 136 },
            {"weight": [52001,54000], "V1": 116, "Vr": 131, "V2": 134 },{"weight": [40000,52000], "V1": 116, "Vr": 126, "V2": 131 }
            ]

            Flap2 = [{"weight": [72001,100000], "V1": 132, "Vr": 147, "V2": 150 }, {"weight": [67001,72000], "V1": 127, "Vr": 142, "V2": 145 }, 
            {"weight": [63001,67000], "V1": 123, "Vr": 138, "V2": 141 },{"weight": [59001,63000], "V1": 119, "Vr": 134, "V2": 137 },
            {"weight": [56001,59000], "V1": 115, "Vr": 130, "V2": 133 },{"weight": [54001,56000], "V1": 114, "Vr": 128, "V2": 134 },
            {"weight": [52001,54000], "V1": 114, "Vr": 128, "V2": 133 },{"weight": [40000,52000], "V1": 114, "Vr": 123, "V2": 132 }
            ]

            Flap3 = [{"weight": [72001,100000], "V1": 130, "Vr": 144, "V2": 147 }, {"weight": [67001,72000], "V1": 125, "Vr": 139, "V2": 142 }, 
            {"weight": [63001,67000], "V1": 121, "Vr": 135, "V2": 138 },{"weight": [59001,63000], "V1": 118, "Vr": 132, "V2": 135 },
            {"weight": [56001,59000], "V1": 113, "Vr": 130, "V2": 133 },{"weight": [54001,56000], "V1": 113, "Vr": 128, "V2": 133 },
            {"weight": [40000,54000], "V1": 113, "Vr": 126, "V2": 131 }
            ]

            try:
                weight1=float(weightentry.get())
                FlapsInput=int(FlapsMaping[FlapsOptions.get()])
                posicion = 0
                PosicionDeseada = 0

                if (FlapsInput == 1):
                    for options in Flap1:
                        resultado = options["weight"]
                        if weight1 in range(resultado[0], resultado[1]):
                            PosicionDeseada = posicion
                        posicion += 1
                    
                    v1=Flap1[PosicionDeseada]["V1"]
                    vr=Flap1[PosicionDeseada]["Vr"]
                    v2=Flap1[PosicionDeseada]["V2"]

                elif (FlapsInput == 2):
                    for options in Flap2:
                        resultado = options["weight"]
                        if weight1 in range(resultado[0], resultado[1]):
                            PosicionDeseada = posicion
                        posicion += 1

                    v1=Flap2[PosicionDeseada]["V1"]
                    vr=Flap2[PosicionDeseada]["Vr"]
                    v2=Flap2[PosicionDeseada]["V2"]

                elif (FlapsInput == 3):
                    for options in Flap3:
                        resultado = options["weight"]
                        if weight1 in range(resultado[0], resultado[1]):
                            PosicionDeseada = posicion
                        posicion += 1

                    v1=Flap3[PosicionDeseada]["V1"]
                    vr=Flap3[PosicionDeseada]["Vr"]
                    v2=Flap3[PosicionDeseada]["V2"]

                v1Entry=Entry(a320_window, font="Arial 16",text=("V1"), fg="white",bg=black_theme, width=3)
                v1Entry.insert(0, v1)
                v1Entry.config(state=DISABLED)
                v1Entry.place(x=150, y=500)

                vrEntry=Entry(a320_window, font="Arial 16",text=("VR"), fg="white",bg=black_theme, width=3)
                vrEntry.insert(0, vr)
                vrEntry.config(state=DISABLED)
                vrEntry.place(x=250, y=500)

                v2Entry=Entry(a320_window, font="Arial 16",text=("V2"), fg="white",bg=black_theme, width=3)
                v2Entry.insert(0, v2)
                v2Entry.config(state=DISABLED)
                v2Entry.place(x=350, y=500)
                

            except:
                tkMessageBox.askretrycancel(title="Try Again", message="The provided data is incorrect")

        FlapsOptions = ttk.Combobox(a320_window, state = 'readonly', values = FlapsList, width=27)
        FlapsOptions.set(FlapsList[0])
        #The next line is used for bind the key and value of the FlapsList and pass it to the function weight_get
        FlapsOptions.bind('<<ComboboxSelected>>', weight_get)
        FlapsOptions.place(x=250, y=300)
        
        #Buttons
        button = Button(a320_window, text="Calculate", command=weight_get, bg="white",fg=black_theme, font="Arial 11").place(x=300, y=370)
        help = Button(a320_window, text="Help", command=tow_help, bg="white",fg=black_theme, font="Arial 7").place(x=450, y=330)

        #Labels
        label = Label(a320_window, text="Flaps Config:",font="Arial 12",bg=black_theme, fg="white").place(x=50, y=300)
        label = Label(a320_window, text="Take Off Weight TOW (Kg):",font="Arial 12",bg=black_theme, fg="white").place(x=50, y=330)

        #Results Entrys and Labels
        result=Label(a320_window, font="Arial 16",text=("V1"), fg="white",bg=black_theme).place(x=155, y=450)
        result=Label(a320_window, font="Arial 16",text=("VR"), fg="white",bg=black_theme).place(x=255, y=450)
        result=Label(a320_window, font="Arial 16",text=("V2"), fg="white",bg=black_theme).place(x=355, y=450)

        v1Entry=Entry(a320_window, font="Arial 16",text=("V1"), fg="white",bg=black_theme, width=3)
        v1Entry.config(state=DISABLED)
        v1Entry.place(x=150, y=500)

        vrEntry=Entry(a320_window, font="Arial 16",text=("VR"), fg="white",bg=black_theme, width=3)
        vrEntry.config(state=DISABLED)
        vrEntry.place(x=250, y=500)

        v2Entry=Entry(a320_window, font="Arial 16",text=("V2"), fg="white",bg=black_theme, width=3)
        v2Entry.config(state=DISABLED)
        v2Entry.place(x=350, y=500)   

    velocity()