# All needed liveries import
from tkinter import *
from tkinter import ttk
from tkinter import messagebox as tkMessageBox
import tkinter.font as tkFont
from tkinter.simpledialog import askinteger
import math
from math import sqrt
import os


def tod_toc():
    # Colors Theme
    black_theme = ("#212121")
    pink = ("#9f328c")

    tod_toc_window = Tk()
    tod_toc_window.title("Top Of Descent/Climb Calculator")
    tod_toc_window.geometry("920x450")
    tod_toc_window.configure(bg=black_theme)
    tod_toc_window.resizable(False, False)

    try:
        tod_toc_window.iconbitmap("images/favicon.ico")

    except:
            print ("Not in windows")


    label = Label(tod_toc_window, text=("Top Of Descent / Climb Calculator"), font="LucidaGrande 24 bold",bg=black_theme, fg=pink).place(x=200, y=50)

    def tod():
        label = Label(tod_toc_window, text=("Top Of Descent"), font="LucidaGrande 18 bold",bg=black_theme, fg=pink).place(x=150, y=150)

        xlabel = Label(tod_toc_window, text=("Initial altitude (in feet): "),font="Arial 12", bg=black_theme, fg="white").place(x=50, y=200)
        xentry = Entry(tod_toc_window, font="Arial 12",bg="white")
        xentry.place(x=230, y=200)

        ylabel = Label(tod_toc_window, text="Desired altitude (in feet): ",font="Arial 12", bg=black_theme, fg="white").place(x=50, y=230)
        yentry = Entry(tod_toc_window, font="Arial 12",bg="white")
        yentry.place(x=230, y=230)

        zlabel = Label(tod_toc_window, text=("Vertical Speed (in feet): "),font="Arial 12", bg=black_theme, fg="white").place(x=50, y=260)
        zentry = Entry(tod_toc_window, font="Arial 12",bg="white")
        zentry.place(x=230, y=260)


        '''Calculate Button: The "tod funcion" is the main used for the entire calculation
            we transform the variables into a float, then we calculate the total with the mathematic form. '''

        def tod_calc():
            '''We are using a try function wich means the program will try to get all the variables from the Entry widget, if some Entry is Null (nothing inside)
            the program will get an internal error and will try the exception showing us a error message box.'''

            try:
                xr = float(xentry.get())
                yr = float(yentry.get())
                zr = float(zentry.get())

                total = round(((xr-yr)/zr))

                '''If the total is less or equal to 0 it means that the altitude entered isn't correct so I open 
                            emergent window with error, else we create the Label called result showing the "total"'''

                if (total <= 0):
                    tkMessageBox.askretrycancel(
                        title="Error", message="The provided data is incorrect")
                else:
                    result = Label(tod_toc_window, text=(
                        f"Will take {total} minutes"), font="Arial 12", bg=pink, fg="white").place(x=250, y=335)
                    print(total)

            except:
                tkMessageBox.askretrycancel(
                    title="Try Again", message="Please fill all gaps")

        button = Button(tod_toc_window, text="Calculate", command=tod_calc, bg="white",
                        fg=black_theme, font="Arial 11").place(x=280, y=300)


    def toc():
        label = Label(tod_toc_window, text=("Top Of Climb"), font="LucidaGrande 18 bold",
                      bg=black_theme, fg=pink).place(x=550, y=150)

        xlabel = Label(tod_toc_window, text=("Initial altitude (in feet): "),font="Arial 12", bg=black_theme, fg="white").place(x=500, y=200)
        xentry = Entry(tod_toc_window, font="Arial 12",bg="white")
        xentry.place(x=680, y=200)

        ylabel = Label(tod_toc_window, text="Desired altitude (in feet): ",font="Arial 12", bg=black_theme, fg="white").place(x=500, y=230)
        yentry = Entry(tod_toc_window, font="Arial 12",bg="white")
        yentry.place(x=680, y=230)

        zlabel = Label(tod_toc_window, text=("Vertical Speed (in feet): "),font="Arial 12", bg=black_theme, fg="white").place(x=500, y=260)
        zentry = Entry(tod_toc_window, font="Arial 12",bg="white")
        zentry.place(x=680, y=260)

        '''Calculate Button: The "tod funcion" is the main used for the entire calculation
            we transform the variables into a float, then we calculate the total with the mathematic 
            form.'''

        def toc_calc():
            '''We are using a try function wich means the program will try to get all the variables from the Entry widget, if some Entry is Null (nothing inside)
            the program will get an internal error and will try the exception showing us a error message box.'''

            try:
                xr = float(xentry.get())
                yr = float(yentry.get())
                zr = float(zentry.get())

                total = round(((yr-xr)/zr))

                '''If the total is less or equal to 0 it means that the altitude entered isn't correct so I open 
                    emergent window with error, else we create the Label called result showing the "total"'''

                if (total <= 0):
                    tkMessageBox.askretrycancel(
                        title="Try Again", message="The provided data is incorrect")
                else:
                    result = Label(tod_toc_window, text=(
                        f"Will take {total} minutes"), font="Arial 12", bg=pink, fg="white").place(x=710, y=335)
                    print(total)

            except:
                tkMessageBox.askretrycancel(
                    title="Try Again", message="Please fill all gaps")

        button = Button(tod_toc_window, text="Calculate", command=toc_calc, bg="white",
                        fg=black_theme, font="Arial 11").place(x=730, y=300)

    tod()
    toc()
