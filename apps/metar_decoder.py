# from tkinter import *
import tkinter.ttk as ttk
from tkinter import messagebox as tkMessageBox
import tkinter as tk
import requests
import json

request_nbr = 0
entries = []


def metar_decoder():
    # Colors Theme
    black_theme = "#212121"
    light_theme = "#353535"
    pink = "#9f328c"

    # Window Configuration
    metar_window = tk.Tk()
    metar_window.title("Metar Decoder")
    metar_window.geometry("820x550")
    metar_window.configure(bg=black_theme)
    metar_window.resizable(False, False)

    try:
        metar_window.iconbitmap("images/favicon.ico")

    except: 
        print("Not in windows")

    def make_entry(x, y):
        """
        return a model entry used to make all of the fields
        pre: x and y are integers representing the coordinates
        post: returns an entry object
        """
        # entry = Label(metar_window, width=40, height=1)
        entry = tk.Label(metar_window, font="Calibri 12", bg=black_theme, fg="white")

        # entry = Entry(metar_window, font="Arial 12", fg="black", bg="white")
        # entry.bind("<Button-1>", lambda e: "break")
        # entry.bind("<Key>", lambda e: "break")
        # entry.config(wrap=WORD)
        # entry.pack(side="top", fill="both", expand=True, padx=0, pady=0)
        entry.place(x=x, y=y)
        entries.append(entry)
        return entry

    def make_label(text, x, y):
        """
        return a model label used to make all of the fields
        pre: text is a string used for the label text, x and y are integers representing the coordinates
        post: returns a label object
        """
        label = tk.Label(metar_window, text=text, font="Arial 12 bold", bg=black_theme, fg="white")
        label.place(x=x, y=y)
        return label

    def clear_entries():
        """
        clears all the text widgets
        pre: entries is a list with text widgets
        post: clears all of the entries
        """
        for entry in entries:
            entry.text = ""
            # entry.delete(1.0, END)
            # entry.update()

    def fetch_metar():
        y_coords = 150
        x_coords = 70
        label = tk.Label(metar_window, text="Metar Decoder", font="LucidaGrande 24 bold",
                         bg=black_theme, fg=pink).place(x=275, y=20)

        metarLabel = tk.Label(metar_window, text="Airport ICAO:", font="Arial 12 bold", bg=black_theme, fg="white")
        metarLabel.place(x=20, y=70)

        metarEntry = tk.Entry(metar_window, font="Arial 12", bg="white", width=5)
        metarEntry.place(x=y_coords, y=70)

        rawLabel = make_label("Raw Metar:", 20, 100)
        rawEntry = make_entry(y_coords, 100)

        stationLabel = make_label("Station:", 20, 130)
        stationEntry = make_entry(y_coords, 130)

        observedLabel = make_label("Observed at:", 20, 160)
        observedEntry = make_entry(y_coords, 160)

        windsLabel = make_label("Winds:", 20, 190)
        windsEntry = make_entry(y_coords, 190)

        visibilityLabel = make_label("Visibility:", 20, 220)
        visibilityEntry = make_entry(y_coords, 220)

        cloudsLabel = make_label("Clouds:", 20, 250)
        cloudsEntry = make_entry(y_coords, 250)

        temperatureLabel = make_label("Temperature:", 20, 280)
        temperatureEntry = make_entry(y_coords, 280)

        dewpointLabel = make_label("Dewpoint:", 20, 310)
        dewpointEntry = make_entry(y_coords, 310)

        altimeterFAALabel = make_label("Altimeter (inHg):", 20, 340)
        altimeterFAAEntry = make_entry(y_coords, 340)

        altimeterLabel = make_label("Altimeter (Hpa):", 20, 370)
        altimeterICAOEntry = make_entry(y_coords, 370)

        def handle_btn():
            """
            Handles the "Get Metar" button, if it's the first request just insert the metar on the text widgets
            otherwise clear the entries and insert the metar
            pre: request_nbr a global var
            post: Fetches the metar and clears the old metar
            """
            global request_nbr
            if request_nbr > 0:
                request_nbr += 1
                clear_entries()
                getIcao()
            else:
                getIcao()
                request_nbr += 1

        def getIcao():
            try:
                icao = metarEntry.get()

                url = 'https://api.checkwx.com/metar/' + str(icao) + '/decoded'
                hdr = {"X-API-Key": '8f463a6216484b239e30008056'}
                req = requests.get(url, headers=hdr)

                try:
                    req.raise_for_status()
                    resp = json.loads(req.text)
                    decoded_data = json.loads(json.dumps(resp, indent=1))
                    print(decoded_data)
                    try:
                        # rawEntry.insert(INSERT, decoded_data['data'][0]['raw_text'])
                        rawEntry['text'] = decoded_data['data'][0]['raw_text']
                        # stationEntry.insert(INSERT, decoded_data['data'][0]['icao'])
                        stationEntry["text"] = decoded_data['data'][0]['icao']
                        # observedEntry.insert(INSERT, f"{decoded_data['data'][0]['observed'][:10]} at {decoded_data[
                        # 'data'][0]['observed'][11:]}")
                        observedEntry[
                            'text'] = f"{decoded_data['data'][0]['observed'][:10]} at {decoded_data['data'][0]['observed'][11:16]} Z"
                        try:
                            # windsEntry.insert(INSERT, f"{decoded_data['data'][0]['wind']['degrees']} @ {
                            # decoded_data['data'][0]['wind']['speed_kts']}")
                            windsEntry[
                                'text'] = f"{decoded_data['data'][0]['wind']['degrees']} @ {decoded_data['data'][0]['wind']['speed_kts']}"
                        except KeyError:
                            # windsEntry.insert(INSERT, "Winds are calm")
                            windsEntry['text'] = "Winds are calm"
                        # visibilityEntry.insert(INSERT, str(decoded_data['data'][0]['visibility']['miles_float']))
                        visibilityEntry['text'] = str(decoded_data['data'][0]['visibility']['miles_float'])

                        for cloud in range(len(decoded_data['data'][0]['clouds'])):
                            code = decoded_data['data'][0]['clouds'][cloud]['code']
                            text = decoded_data['data'][0]['clouds'][cloud]['text']

                            if code == 'CLR' or code == 'CAVOK':
                                # cloudsEntry.insert(INSERT, str(text))
                                cloudsEntry['text'] = text
                            elif code == 'BKN' or code == 'FEW' or code == 'SCT':
                                if cloud < len(decoded_data['data'][0]['clouds']):
                                    # cloudsEntry.insert(INSERT, f"{text} at {decoded_data['data'][0]['clouds'][
                                    # cloud]['feet']}, ")
                                    cloudsEntry[
                                        'text'] = f"{text} at {decoded_data['data'][0]['clouds'][cloud]['feet']}, "
                                else:
                                    # cloudsEntry.insert(INSERT, f"{text} at {decoded_data['data'][0]['clouds'][
                                    # cloud]['feet']}")
                                    cloudsEntry[
                                        'text'] = f"{text} at {decoded_data['data'][0]['clouds'][cloud]['feet']}"

                        # temperatureEntry.insert(INSERT, f"{decoded_data['data'][0]['temperature']['celsius']}°C")
                        temperatureEntry['text'] = f"{decoded_data['data'][0]['temperature']['celsius']}°C"

                        # dewpointEntry.insert(INSERT, f"{decoded_data['data'][0]['dewpoint']['celsius']}°C")
                        dewpointEntry['text'] = f"{decoded_data['data'][0]['dewpoint']['celsius']}°C"
                        # altimeterFAAEntry.insert(INSERT, decoded_data['data'][0]['barometer']['hg'])
                        altimeterFAAEntry['text'] = decoded_data['data'][0]['barometer']['hg']
                        # altimeterICAOEntry.insert(INSERT, decoded_data['data'][0]['barometer']['hpa'])
                        altimeterICAOEntry['text'] = decoded_data['data'][0]['barometer']['hpa']

                    except IndexError as e:
                        print(e)
                        tkMessageBox.askretrycancel(title="Error", message=f"Invalid ICAO, please enter a valid ICAO")
                except requests.exceptions.HTTPError as e:
                    print(e)
            except Exception as e:
                print(e)
                tkMessageBox.askretrycancel(title="Error", message=f"There was an error: {e}")



        # searchBtn = ttk.Button(metar_window, text="Search", command=handle_btn).place(relx=0.255, rely=0.125)

        button = tk.Button(metar_window, text="Search", command=handle_btn, bg=black_theme, fg="white",
                        activebackground=light_theme, activeforeground="white", font="Arial 10").place(relx=0.255, rely=0.124)

    fetch_metar()
