# All needed liveries import
from tkinter import *
from tkinter import ttk
from tkinter import messagebox as tkMessageBox
import tkinter.font as tkFont
from tkinter.simpledialog import askinteger
import math
from math import sqrt
import os

def units_converter():
    # Colors Theme
    black_theme = ("#212121")
    pink = ("#9f328c")

    units_window = Tk()
    units_window.title("Units Converter")
    units_window.geometry("920x750")
    units_window.configure(bg=black_theme)
    units_window.resizable(False, False)


    try:
        units_window.iconbitmap("images/favicon.ico")

    except:
            print ("Not in windows")

    label = Label(units_window, text=("Units Converter"), font="LucidaGrande 24 bold",bg=black_theme, fg=pink).place(x=350, y=50)

    def kmtonm():
        # The main operating value, km/1.852=nm or nm*1.852=km

        # KILOMETRES TO NAUTICAL MILES

        '''
            Here we are creating all diferent Lables and entry's for the program to work, this is mostly visual part
            as we can see, we have 2 diferent entries, 1 (kmentry) is for entring a value in km to convert into nm. The entry 2 called nmentry aims
            to show the result of the operation. We disable it with state=DISABLED
            '''

        label = Label(units_window, text=("Km to Nm Converter"), font="LucidaGrande 18 bold",bg=black_theme, fg=pink).place(x=550, y=150)  # Title label

        kmlabel = Label(units_window, text=("Kilometers: "), font="Arial 12",bg=black_theme, fg="white").place(x=500, y=200)
        nmlabel = Label(units_window, text=("Nautical Miles: "), font="Arial 12",bg=black_theme, fg="white").place(x=500, y=230)

        kmentry = Entry(units_window, font="Arial 12", bg="white")
        kmentry.place(x=680, y=200)
        nmentry = Entry(units_window, font="Arial 12", bg="white")
        nmentry.config(state=DISABLED)
        nmentry.place(x=680, y=230)

        '''Calculate Button: The dist function is the one that calculates all, with a matematic form, 
        this function is compoused by a try, this will try to get the Entry String and convert into a float number.
        Then we will calculate the final result and show it in the "nmentry" Entry box. Except we will show a error message'''

        def dist():
            try:
                v1r = float(kmentry.get())
                total = (v1r/1.852)
                totalrounded = round(total,2)
                nmentry = Entry(units_window, font="Arial 12", bg="white")
                nmentry.place(x=680, y=230)
                nmentry.insert(0, totalrounded)
                nmentry.config(state=DISABLED)
                print(totalrounded)

            except:
                tkMessageBox.askretrycancel(
                    title="Try Again", message="Please provide a valid number")

        # BUTTONS

        button = Button(units_window, text="Calculate", command=dist, bg="white",fg=black_theme, font="Arial 11").place(x=730, y=260)
        #button = Button(units_window, text="Reset", command=resetkm, bg="white",fg=black_theme, font="Arial 11").place(x=740, y=500)

    def nmtokm():
        # The main operating value, km/1.852=nm or nm*1.852=km

        # KILOMETRES TO NAUTICAL MILES

        '''
            Here we are creating all diferent Lables and entry's for the program to work, this is mostly visual part
            as we can see, we have 2 diferent entries, 1 (kmentry) is for entring a value in km to convert into nm. The entry 2 called nmentry aims
            to show the result of the operation. We disable it with state=DISABLED
            '''

        label = Label(units_window, text=("Nm to Km Converter"), font="LucidaGrande 18 bold",bg=black_theme, fg=pink).place(x=130, y=150)  # Title label

        nmlabel = Label(units_window, text=("Nautical Miles: "), font="Arial 12",bg=black_theme, fg="white").place(x=50, y=200)
        kmlabel = Label(units_window, text=("Kilometers: "), font="Arial 12",bg=black_theme, fg="white").place(x=50, y=230)

        nmentry = Entry(units_window, font="Arial 12", bg="white")
        nmentry.place(x=230, y=200)
        kmentry = Entry(units_window, font="Arial 12", bg="white")
        kmentry.config(state=DISABLED)
        kmentry.place(x=230, y=230)

        '''Calculate Button: The dist function is the one that calculates all, with a matematic form, 
        this function is compoused by a try, this will try to get the Entry String and convert into a float number.
        Then we will calculate the final result and show it in the "nmentry" Entry box. Except we will show a error message'''

        def dist():
            try:
                v1r = float(nmentry.get())
                total = (v1r*1.852)
                totalrounded = round(total,2)
                kmentry = Entry(units_window, font="Arial 12", bg="white")
                kmentry.place(x=230, y=230)
                kmentry.insert(0, totalrounded)
                kmentry.config(state=DISABLED)
                print(totalrounded)

            except:
                tkMessageBox.askretrycancel(
                    title="Try Again", message="Please provide a valid number")

        # BUTTONS

        button = Button(units_window, text="Calculate", command=dist, bg="white",fg=black_theme, font="Arial 11").place(x=280, y=260)


    def mbartoinhg():
        # The main operating value, milibar/33.864=inhg

        # MILIBARS TO INHG

        '''
            Here we are creating all diferent Lables and entry's for the program to work, this is mostly visual part
            as we can see, we have 2 diferent entries, 1 (milibarentry) is for entring a value in milibar to convert into inhg. The entry 2 called nmentry aims
            to show the result of the operation. We disable it with state=DISABLED
            '''

        label = Label(units_window, text=("HpA to InHg Converter"), font="LucidaGrande 18 bold",bg=black_theme, fg=pink).place(x=550, y=350)  # Title label

        mbarlabel = Label(units_window, text=("HpA: "), font="Arial 12",bg=black_theme, fg="white").place(x=500, y=400)
        inhglabel = Label(units_window, text=("InHg: "), font="Arial 12",bg=black_theme, fg="white").place(x=500, y=430)

        mbarentry = Entry(units_window, font="Arial 12", bg="white")
        mbarentry.place(x=680, y=400)
        inhgentry = Entry(units_window, font="Arial 12", bg="white")
        inhgentry.config(state=DISABLED)
        inhgentry.place(x=680, y=430)

        '''Calculate Button: The dist function is the one that calculates all, with a matematic form, 
        this function is compoused by a try, this will try to get the Entry String and convert into a float number.
        Then we will calculate the final result and show it in the "nmentry" Entry box. Except we will show a error message'''

        def dist():
            try:
                v1r = float(mbarentry.get())
                total = (v1r/33.864)
                totalrounded=round(total, 2)
                nmentry = Entry(units_window, font="Arial 12", bg="white")
                nmentry.place(x=680, y=430)
                nmentry.insert(0, totalrounded)
                nmentry.config(state=DISABLED)
                print(totalrounded)

            except:
                tkMessageBox.askretrycancel(
                    title="Try Again", message="Please provide a valid number")

        # BUTTONS

        button = Button(units_window, text="Calculate", command=dist, bg="white",fg=black_theme, font="Arial 11").place(x=730, y=460)

    def inhgtombar():
        # The main operating value, km/1.852=nm or nm*1.852=km

        # KILOMETRES TO NAUTICAL MILES

        '''
            Here we are creating all diferent Lables and entry's for the program to work, this is mostly visual part
            as we can see, we have 2 diferent entries, 1 (kmentry) is for entring a value in km to convert into nm. The entry 2 called nmentry aims
            to show the result of the operation. We disable it with state=DISABLED
            '''
        label = Label(units_window, text=("InHg to HpA Converter"), font="LucidaGrande 18 bold",bg=black_theme, fg=pink).place(x=130, y=350)  # Title label

        mbarlabel = Label(units_window, text=("InHg: "), font="Arial 12",bg=black_theme, fg="white").place(x=50, y=400)
        inhglabel = Label(units_window, text=("HpA: "), font="Arial 12",bg=black_theme, fg="white").place(x=50, y=430)

        inhgentry = Entry(units_window, font="Arial 12", bg="white")
        inhgentry.place(x=230, y=400)
        mbarentry = Entry(units_window, font="Arial 12", bg="white")
        mbarentry.config(state=DISABLED)
        mbarentry.place(x=230, y=430)


        '''Calculate Button: The dist function is the one that calculates all, with a matematic form, 
        this function is compoused by a try, this will try to get the Entry String and convert into a float number.
        Then we will calculate the final result and show it in the "nmentry" Entry box. Except we will show a error message'''

        def dist():
            try:
                v1r = float(inhgentry.get())
                total = (v1r*33.864)
                totalrounded=round(total)
                nmentry = Entry(units_window, font="Arial 12", bg="white")
                nmentry.place(x=230, y=430)
                nmentry.insert(0, totalrounded)
                nmentry.config(state=DISABLED)
                print(totalrounded)

            except:
                tkMessageBox.askretrycancel(
                    title="Try Again", message="Please provide a valid number")

        # BUTTONS

        button = Button(units_window, text="Calculate", command=dist, bg="white",fg=black_theme, font="Arial 11").place(x=280, y=460)

    def kgtolbs():

        # KILOGRAMS TO POUNDS

        label = Label(units_window, text=("Kg to Lbs Converter"), font="LucidaGrande 18 bold",bg=black_theme, fg=pink).place(x=550, y=550)  # Title label

        kgslabel = Label(units_window, text=("Kg: "), font="Arial 12",bg=black_theme, fg="white").place(x=500, y=600)
        lbslabel = Label(units_window, text=("Lbs: "), font="Arial 12",bg=black_theme, fg="white").place(x=500, y=630)

        kgsentry = Entry(units_window, font="Arial 12", bg="white")
        kgsentry.place(x=680, y=600)
        lbsentry = Entry(units_window, font="Arial 12", bg="white")
        lbsentry.config(state=DISABLED)
        lbsentry.place(x=680, y=630)

        '''Calculate Button: The dist function is the one that calculates all, with a matematic form, 
        this function is compoused by a try, this will try to get the Entry String and convert into a float number.
        Then we will calculate the final result and show it in the "nmentry" Entry box. Except we will show a error message'''

        def dist():
            try:
                v1r = float(kgsentry.get())
                total = (v1r*2.205)
                totalrounded=round(total)
                lbsentry = Entry(units_window, font="Arial 12", bg="white")
                lbsentry.place(x=680, y=630)
                lbsentry.insert(0, totalrounded)
                lbsentry.config(state=DISABLED)
                print(totalrounded)

            except:
                tkMessageBox.askretrycancel(
                    title="Try Again", message="Please provide a valid number")

        # BUTTONS

        button = Button(units_window, text="Calculate", command=dist, bg="white",fg=black_theme, font="Arial 11").place(x=730, y=660)

    def lbstokg():

        # POUNDS TO KILOGRAMS

        label = Label(units_window, text=("Lbs to Kg Converter"), font="LucidaGrande 18 bold",bg=black_theme, fg=pink).place(x=130, y=550)  # Title label

        kgslabel = Label(units_window, text=("Kg: "), font="Arial 12",bg=black_theme, fg="white").place(x=50, y=600)
        lbslabel = Label(units_window, text=("Lbs: "), font="Arial 12",bg=black_theme, fg="white").place(x=50, y=630)

        kgsentry = Entry(units_window, font="Arial 12", bg="white")
        kgsentry.place(x=230, y=600)
        lbsentry = Entry(units_window, font="Arial 12", bg="white")
        lbsentry.config(state=DISABLED)
        lbsentry.place(x=230, y=630)

        '''Calculate Button: The dist function is the one that calculates all, with a matematic form, 
        this function is compoused by a try, this will try to get the Entry String and convert into a float number.
        Then we will calculate the final result and show it in the "nmentry" Entry box. Except we will show a error message'''

        def dist():
            try:
                v1r = float(kgsentry.get())
                total = (v1r/2.205)
                totalrounded=round(total)
                lbsentry = Entry(units_window, font="Arial 12", bg="white")
                lbsentry.place(x=230, y=630)
                lbsentry.insert(0, totalrounded)
                lbsentry.config(state=DISABLED)
                print(totalrounded)

            except:
                tkMessageBox.askretrycancel(
                    title="Try Again", message="Please provide a valid number")

        # BUTTONS

        button = Button(units_window, text="Calculate", command=dist, bg="white",fg=black_theme, font="Arial 11").place(x=280, y=660)

    kmtonm()
    nmtokm()
    mbartoinhg()
    inhgtombar()
    kgtolbs()
    lbstokg()